<!--@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Introduction to Software Safety
    @parent
@stop

{{-- SEO Headers --}}
@section('seo_headers')
    <meta name="description" content="Summer REU: Software Safety and Reliability: Research and Application">
@stop

{{-- content --}}
@section('body')-->
    <!-- HEADER -->
    <header class="header-cat">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="testi-head">Lecture on Technical Communication</h1>
            </div>
        </div>
	<style>
	.container {
		padding: 0 10% !important;
	}
	.video-container {
		position: relative;
		width: 100%;
		height: 0;
		padding-bottom: 56.25%;
	}
	.video {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
	}
	</style>
    </header>

<!-- Main Contain Section -->
<div class="main-container">
    <section id="page" class="page">
        <div class="container">
            <!-- content start -->
            <center><h4><hr><strong>
Presented for the REU (Research Experience for Undergraduates)<br> 
Summer Program at the University of Texas at Dallas</strong></h4>

<br><strong>
PI: Professor <a href="http://www.utdallas.edu/~ewong" target="_blank">W. Eric Wong</a><br>
Department of Computer Science<br>
<br>
Special Acknowledgment: The Writing Center, Office of Student Success and Assessment</strong></center><center><hr></center><br>


<h3 style="text-align: left;"><span style="color: rgb(41, 144, 255); font-family: " noto="" sans",="" serif;="" font-size:="" 1.75rem;"="">Part I
</span><a href="https://www.youtube.com/watch?v=r8IUU0SlW08&amp;list=PLfSlwQL0cI3jxrmuIh6vE_6n4mS0iCY4M" target="_blank" style="font-family: " noto="" sans",="" serif;="" font-size:="" 1.75rem;="" background-color:="" rgb(255,="" 255,="" 255);"="">
<img src="./playlist-icon.png" height="24px" border="0" alt=""></a></h3>
     
<ul>
<li>Introduction
         <a href="https://youtu.be/r8IUU0SlW08" target="_blank">
         <img src="./camcoder-icon.jpg" height="12px" border="0" alt="">
	 </a>
     </li>
<li>What is Technical Communication
         <a href="https://youtu.be/mBUHPzWdfVY" target="_blank">
         <img src="./camcoder-icon.jpg" height="12px" border="0" alt="">
	 </a>
     </li>
<li>Argument part 1
         <a href="https://youtu.be/2o6ay8fIMvo" target="_blank">
         <img src="./camcoder-icon.jpg" height="12px" border="0" alt="">
	 </a>
     </li>
<li>Models
         <a href="https://youtu.be/5hytZY3j5IE" target="_blank">
         <img src="./camcoder-icon.jpg" height="12px" border="0" alt="">
	 </a>
     </li>
<li>Argument part 2
         <a href="https://youtu.be/R5TGaVbv14A" target="_blank">
         <img src="./camcoder-icon.jpg" height="12px" border="0" alt="">
	 </a>
     </li>
<li>Heuristics
         <a href="https://youtu.be/-rmoEw5Ab3g" target="_blank">
         <img src="./camcoder-icon.jpg" height="12px" border="0" alt="">
	 </a>
     </li>
<li>Rhetoric
         <a href="https://youtu.be/unZce628yXU" target="_blank">
         <img src="./camcoder-icon.jpg" height="12px" border="0">
	 </a>
</li></ul>

<!----------------------------------------------------------------------------------------->
<h3 style="color:#2990ff;">Part II
<a href="https://www.youtube.com/watch?list=PLfSlwQL0cI3iiks3y9FI-NQa7CcD-mKb-&amp;v=k1lQj6Fur38" target="_blank" style="font-family: " noto="" sans",="" serif;="" font-size:="" 1.75rem;="" background-color:="" rgb(255,="" 255,="" 255);"="">
<img src="./playlist-icon.png" height="24px" border="0" alt=""></a><br></h3>

<ul>
<li> Types of Technical Communication
          <a href="https://youtu.be/k1lQj6Fur38" target="_blank">
          <img src="./camcoder-icon.jpg" height="12px" border="0">
	  </a>
     </li>
<li> Technical Communication Models
          <a href="https://youtu.be/PSCh0hpNUGk" target="_blank">
          <img src="./camcoder-icon.jpg" height="12px" border="0">
	  </a>
     </li>
<li> DITA: a Model for Content Reuse
          <a href="https://youtu.be/pZ1c7XHpIk8" target="_blank">
          <img src="./camcoder-icon.jpg" height="12px" border="0">
	  </a>
     </li>
<li> Heuristics for Better Writing
          <a href="https://youtu.be/hAH4-Sjju6I" target="_blank">
          <img src="./camcoder-icon.jpg" height="12px" border="0">
	  </a>
     </li>
<li> Standards for Usability Accessibility and Reusability
          <a href="https://youtu.be/Wa_-xjDuhZU" target="_blank">
          <img src="./camcoder-icon.jpg" height="12px" border="0">
	  </a>
     </li>
<li> Writing is Difficult
          <a href="https://youtu.be/n4RSO9i73qA" target="_blank">
          <img src="./camcoder-icon.jpg" height="12px" border="0">
	  </a>
     </li>
<li> Enforced Standards and Automated Content
          <a href="https://youtu.be/VmUXb_BiJF8" target="_blank">
          <img src="./camcoder-icon.jpg" height="12px" border="0">
	  </a>
     </li>
<li> Corporate Customer and Competing Standards
          <a href="https://youtu.be/FBuJB0KnKF8" target="_blank">
          <img src="./camcoder-icon.jpg" height="12px" border="0">
	  </a>
     </li>
<li> Implicit and Explicit Standards
          <a href="https://youtu.be/LpZjaDXe8p8" target="_blank">
          <img src="./camcoder-icon.jpg" height="12px" border="0">
	  </a>
     </li>
<li> Docbook SGML and XML
          <a href="https://youtu.be/GFFW8Y6TWYM" target="_blank">
          <img src="./camcoder-icon.jpg" height="12px" border="0">
	  </a>
     </li>
<li> Standards Organizations
          <a href="https://youtu.be/WLb4ACfI6lM" target="_blank">
          <img src="./camcoder-icon.jpg" height="12px" border="0">
	  </a>
     </li>
<li> Standards in Domains Military Regulatory and Industry
          <a href="https://youtu.be/WGkdVdL0GWA" target="_blank">
          <img src="./camcoder-icon.jpg" height="12px" border="0">
	  </a>
     </li>
<li> Standards for Style format and Writing
          <a href="https://youtu.be/sMQdvQ8gCXU" target="_blank">
          <img src="./camcoder-icon.jpg" height="12px" border="0">
	  </a>
     </li>
<li> Ethos and Bias
          <a href="https://youtu.be/fQkVdCyOcOQ" target="_blank">
          <img src="./camcoder-icon.jpg" height="12px" border="0">
	  </a>
     </li>
<li> Process Plan Write and Iterate
          <a href="https://youtu.be/e8rdPwzF2wo" target="_blank">
          <img src="./camcoder-icon.jpg" height="12px" border="0">
	  </a>
     </li>
<li> How do I know when I am done
          <a href="https://youtu.be/RYP3ydAZUzk" target="_blank">
          <img src="./camcoder-icon.jpg" height="12px" border="0">
	  </a>
     </li>
<li> Waterfall and Agile Project Management
          <a href="https://youtu.be/dsa3DW34rgY" target="_blank">
          <img src="./camcoder-icon.jpg" height="12px" border="0">
	  </a>
     </li>
<li> Example Analysis of a Blog Post
          <a href="https://youtu.be/EOZspdosr6c" target="_blank">
          <img src="./camcoder-icon.jpg" height="12px" border="0">
	  </a>
     </li>
<li> For More Information
          <a href="https://youtu.be/k5gav5IONiA" target="_blank">
          <img src="./camcoder-icon.jpg" height="12px" border="0">
	  </a>
     </li>
<li> Closing Thoughts
          <a href="https://youtu.be/37I2swiprxc" target="_blank">
          <img src="./camcoder-icon.jpg" height="12px" border="0">
	  </a></li>
</ul>
            <!-- content end -->
        </div>
    </section>
</div>
@stop
