<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>SE4485 Software Engineering Project Home Page</title>
    <link rel="stylesheet" href="cseet.css" type="text/css" />

    <style>
        * {
            font-family: "Times New Roman", Georgia, Serif;
        }
    </style>
    <style id="style-1-cropbar-clipper">
        /* Copyright 2014 Evernote Corporation. All rights reserved. */
        .en-markup-crop-options {
            top: 18px !important;
            left: 50% !important;
            margin-left: -100px !important;
            width: 200px !important;
            border: 2px rgba(255, 255, 255, .38) solid !important;
            border-radius: 4px !important;
        }

        .en-markup-crop-options div div:first-of-type {
            margin-left: 0px !important;
        }

        ul {
            list-style-position: outside;
            margin-left: -25px;
        }
    </style>
    <style>
        .schedule td {
            padding: 2px;
            padding-right: 12px;
            text-align: left;
        }

        ul.relative {
            position: relative;
            left: -24px;
        }
    </style>
</head>

<body>
    <?php

    $lines = file('ps');
    $credentials = array();

    foreach ($lines as $line) {
        if (empty($line)) continue;

        // whole line
        $line = trim(str_replace(": ", ':', $line));
        $lineArr = explode(' ', $line);

        // username only
        $username = explode(':', $lineArr[0]);
        $username = array_pop($username);

        // password
        $password = explode(':', $lineArr[1]);
        $password = array_pop($password);

        // putting them together
        $credentials[$username] = $password;
    }

    $user = $_POST['username'];
    $pass = $_POST['password'];

    if ((array_key_exists($user, $credentials)) && ($credentials[$user] == $pass)) {
    ?>

        <center><b>
                <font size="+2"> </font>
            </b>
            <br><b>
                <font size="+2">SE 4485: Software Engineering Project</font>
            </b>
            <br>
            <br><b>
                <font size="+2">Spring 2024</font>
            </b><br>
            <br><b>
                <font>1:00 pm - 3:45 pm, Friday, ECSW 1.365</font>
            </b>

        </center>
        <!------------------------------------------------------------------------------->
        <p><b>
                <font color="#3333FF">
                    <font size="+1">Instructor</font>
                </font>
            </b></p>
        <blockquote>
			<b>W. Eric Wong</b>
            <!--<b><a href="http://paris.utdallas.edu/ewong/" target="_blank">W. Eric Wong</a></b>-->
            <br><b>Office: ECSS 3.224</b>
            <br><b>Email: <a href="mailto:ewong@utdallas.edu">ewong@utdallas.edu</a></b>
            <br><b>Office Hours: 9:00 am - 10:00 am, Friday or by appointment
            </b>
        </blockquote><b>
            <!--------------------------------------------------------------------------------------->
            <p><b>
                    <font color="#3333FF">
                        <font size="+1">Teaching Assistant</font>
                    </font>
                </b></p>
            <blockquote>
                <!-- <b>TBA</b> -->
                <b>Zizhao Chen</b>
                <br>
                <b>Office: ECSS 3.216</b>
                <br>
                <b>Email: <a href="mailto:zxc190007@utdallas.edu">zxc190007@utdallas.edu</a></b>
                <br>
                <b>Office Hours: 2:00 pm - 3:00 pm, Friday or by appointment</b>
                <br>
                <br>
                <b>Chih-Wei Hsu</b>
                <br>
                <b>Office: ECSS 3.216</b>
                <br>
                <b>Email: <a href="mailto:cxh210019@utdallas.edu">cxh210019@utdallas.edu</a></b>
                <br>
                <b>Office Hours: 2:00 pm - 3:00 pm, Friday or by appointment</b>
            </blockquote><b>
                <!------------------------------------------------------------------------------>
                <p><b>
                        <font color="#3333FF">
                            <font size="+1">Text Book</font>
                        </font>
                    </b></p>
                <blockquote>
                    <b> No official textbook
                    </b>
                </blockquote><b>
                    <!------------------------------------------------------------------------------->
                    <p><b>
                            <font color="#3333FF">
                                <font size="+1">Course Description </font>
                            </font>
                        </b></p>
                    <blockquote>
                        <b>This course is intended to complement the theory and to provide an in-depth,
                            <br>hands-on experience in all aspects of software engineering. The students will
                            <br>work in teams on projects of interest to the industry and will be involved in
                            <br>the analysis of requirements, architecture and design, implementation, testing
                            <br>and validation, project management, software process, and software maintenance.
                        </b>
                    </blockquote>
                    <!------------------------------------------------------------------------------->
                    <p><b>
                            <font color="#3333FF">
                                <font size="+1">Course Learning Outcomes </font>
                            </font>
                        </b>
                    </p>
                    <blockquote>
                        <ul>
                            <li>Ability to define a project plan and track deviation from this plan</li>
                            <li>Ability to properly choose a software process/development methodology and implement it with the available personnel</li>
                            <li>Ability to use a configuration management system and develop team CM processes</li>
                            <li>Ability to work effectively and responsibly with others in a team development environment</li>
                            <li>Ability to create test cases using Scenarios</li>
                            <li>Ability to create and use a traceability matrix between requirements and artifacts</li>
                            <li>Ability to develop progress reports and present/demonstrate a software product to customers</li>
                            <li>Ability to analyze the impact of a project on public health, safety, and welfare</li>
                            <li>Ability to include global, cultural, social, environmental, and economic factors in project development</li>
                            <li>Ability to independently research and learn new programming languages, platforms, and/or design approaches required to develop industrial applications</li>
                        </ul>
                    </blockquote>
                    <!------------------------------------------------------------------------------->
                    <p><b>
                            <font color="#3333FF">
                                <font size="+1">Course Syllabus </font>
                            </font>
                        </b></p>
                    <blockquote>
                        <li>
                            <a href="./Syllabus/SE4485-Syllabus-2024-Spring.pdf" target="_blank">SE4485 Course Outline</a>
                            <!-- <a href="" target="_blank">TBA</a> -->
                        </li>
                    </blockquote>
                    <!------------------------------------------------------------------------------->

                    <!-- <p><b><font color="#3333FF"><font size="+1">Attendance Policy</font></font></b>
   </p><blockquote>
   <li>Absence in three consecutive lectures will result in the course grade being lowered by one letter. </li>
   <li>Absence in four consecutive lectures will automatically result in a failing grade (F) in the course.</li>
   <br>
   Click 
   <a href="http://cs.utdallas.edu/education/undergraduate/attendance-policy/"> here</a>
   for more details.
   <br>
   </blockquote> -->
                    <!------------------------------------------------------------------------------->

                    <p><b>
                            <font color="#3333FF">
                                <font size="+1">Kickoff Meeting</font>
                            </font>
                        </b>
                    </p>
                    <blockquote>
                        <ul>
                            <!-- <li><font color="red">August 27, Friday, 1:00 - 3:45 pm, in-person, ECSS 2.102 (TI Auditorium)</font><br></li>
        <li>Kickoff Meeting Agenda [<a href="Kickoff-Meeting/Agenda.pdf">pdf</a>]<br></li> -->

                            <li>
                                <font color="red">Friday, January 19 (1:00 pm - 4:00 pm, CDT), 2024 in ECSW 1.365 </font>
                                [<a href="Kickoff-Meeting/SE4485-schedule.pdf" target="_blank">Schedule</a>]
                                <br>
                            </li>
                            <!-- <li>Kickoff Meeting Agenda [<a href="">TBA</a>]<br></li> -->
                        </ul>
                    </blockquote>
                    <!------------------------------------------------------------------------------->
                    <p><b>
                            <font color="#3333FF">
                                <font size="+1">Projects</font>
                            </font>
                        </b>
                    </p>

                    <blockquote>

                        <ul>
                            <li>Predictive Analytics Application<br>
                                Sponsored by <font color="#800080">Raytheon</font><br>
                                <b>Industrial Mentor:</b>
                                <font color="green">Marc Perna and Darryl Nelson</font><br>
                                <!-- [<a href="Projects/SE4485-Marc.pdf" target="_blank">Description</a>]
                                [<a href="Projects/Build CIObrain [asset management] Web Application - Slides - Tom Hill.pdf">Slides</a>] -->
                            </li><br>
                            <li>Create a Utility to Transfer VSCode Environments between Windows Systems<br>
                                Sponsored by <font color="#800080">Forcepoint</font><br>
                                <b>Industrial Mentor:</b>
                                <font color="green">Dustin Endres</font><br>
                                <!-- [<a href="Projects/SE4485-Dustin.pdf" target="_blank">Description</a>]
                                 [<a href="Projects/Build CIObrain [asset management] Web Application - Slides - Tom Hill.pdf">Slides</a>] -->
                            </li><br>
                            <li>uARexpert Phase II Aggregate Shopping Cart<br>
                                Sponsored by <font color="#800080">uARexpert</font><br>
                                <b>Industrial Mentor:</b>
                                <font color="green">Dave Gibson and Bryan Weisinger</font><br>
                                <!-- [<a href="Projects/SE4485-Dave.pdf" target="_blank">Description</a>]
                                [<a href="Projects/Build CIObrain [asset management] Web Application - Slides - Tom Hill.pdf">Slides</a>] -->
                            </li><br>
                            <li>Exterior House Structure Analysis - Phase I<br>
                                Sponsored by <font color="#800080">Dakota Farms Realty, LLC</font><br>
                                <b>Industrial Mentor:</b>
                                <font color="green">Corey Wick and Dave Gibson</font><br>
                                <!-- [<a href="Projects/SE4485-Corey.pdf" target="_blank">Description</a>]
                                [<a href="Projects/Build CIObrain [asset management] Web Application - Slides - Tom Hill.pdf">Slides</a>] -->
                            </li><br>
                            <li>Multi-LLM Agile Assistant - Phase II<br>
                                Sponsored by <font color="#800080">ARGO</font><br>
                                <b>Industrial Mentor:</b>
                                <font color="green">Mark Bentsen</font><br>
                                <!-- [<a href="Projects/SE4485-Mark1.pdf" target="_blank">Description</a>]
                                [<a href="Projects/Build CIObrain [asset management] Web Application - Slides - Tom Hill.pdf">Slides</a>] -->
                            </li><br>
                            <li>Storybook POC Continuation with Chromatic and Storybook GPT<br>
                                Sponsored by <font color="#800080">ARGO</font><br>
                                <b>Industrial Mentor:</b>
                                <font color="green">Mark Bentsen, Ponchai Reainthong, Kevin Roa, and Raisa Gonzalez</font><br>
                                <!-- [<a href="Projects/SE4485-Mark2.pdf" target="_blank">Description</a>]
                                [<a href="Projects/Build CIObrain [asset management] Web Application - Slides - Tom Hill.pdf">Slides</a>] -->
                            </li><br>
                            <li>Design, Develop and Test a Simple App to Show How Difficult It Is to Find Malware on a Windows-PC<br>
                                Sponsored by <font color="#800080">The Fellows Consulting Group {FCG}</font><br>
                                <b>Industrial Mentor:</b>
                                <font color="green">Tom Hill</font><br>
                                <!-- [<a href="Projects/SE4485-Tom1.pdf" target="_blank">Description</a>]
                                [<a href="Projects/Build CIObrain [asset management] Web Application - Slides - Tom Hill.pdf">Slides</a>] -->
                            </li><br>
                            <li>Design, Develop and Test a Simulator App for the Windows-Version of the Intrusion Detection App SNORT<br>
                                Sponsored by <font color="#800080">The Fellows Consulting Group {FCG}</font><br>
                                <b>Industrial Mentor:</b>
                                <font color="green">Tom Hill</font><br>
                                <!-- [<a href="Projects/SE4485-Tom2.pdf" target="_blank">Description</a>]
                                [<a href="Projects/Build CIObrain [asset management] Web Application - Slides - Tom Hill.pdf">Slides</a>] -->
                            </li><br>

                            <li>3D Satcom Mapping<br>
                                Sponsored by <font color="#800080">Communications and Power Industries</font><br>
                                <b>Industrial Mentor:</b>
                                <font color="green">Luke Carr</font><br>
                                <!-- [<a href="Projects/SE4485-Luke1.pdf" target="_blank">Description</a>]
                                [<a href="Projects/Build CIObrain [asset management] Web Application - Slides - Tom Hill.pdf">Slides</a>] -->
                            </li><br>
                            <li>Satcom UI Enhancements<br>
                                Sponsored by <font color="#800080">Communications and Power Industries</font><br>
                                <b>Industrial Mentor:</b>
                                <font color="green">Luke Carr</font><br>
                                <!-- [<a href="Projects/SE4485-Luke2.pdf" target="_blank">Description</a>]
                                [<a href="Projects/Build CIObrain [asset management] Web Application - Slides - Tom Hill.pdf">Slides</a>] -->
                            </li><br>
                            <li>Internet Research Assistant <br>
                                Sponsored by <font color="#800080">Independent Consultant</font><br>
                                <b>Industrial Mentor:</b>
                                <font color="green">Jeff Buchmiller</font><br>
                                <!-- [<a href="Projects/SE4485-Jeff.pdf" target="_blank">Description</a>]
                                [<a href="Projects/Build CIObrain [asset management] Web Application - Slides - Tom Hill.pdf">Slides</a>] -->
                            </li><br>

                            <!--<li>SATCOM Wiki - Markdown<br>
                                Sponsored by <font color="#800080">Communications and Power Industries</font><br>
                                <b> Industrial Mentor:</b>
                                <font color="green">Luke Carr</font><br>
                                [<a href="Projects/SE4485-Luke.pdf">Description</a>]
                                [<a href="Projects/Build CIObrain [asset management] Web Application - Slides - Tom Hill.pdf">Slides</a>] 
                            </li><br>

                            <li>SATCOM App - Android<br>
        Sponsored by <font color="#800080">Communications and Power Industries</font><br>
        <b> Industrial Mentor:</b> <font color="green">Luke Carr</font><br>
        [<a href="Projects/SATCOM-App-Android-Luke-Carr.pdf">Description</a>]
        [<a href="Projects/Build CIObrain [asset management] Web Application - Slides - Tom Hill.pdf">Slides</a>] 
    </li><br>

                            <li>Market Awareness Analytics Application<br>
                                Sponsored by <font color="#800080">Raytheon Technologies</font><br>
                                <b> Industrial Mentors:</b>
                                <font color="green">Marc Perna and Darryl Nelson</font><br>
                                [<a href="Projects/SE4485-Marc.pdf">Description</a>]
                                [<a href="Projects/Build CIObrain [asset management] Web Application - Slides - Tom Hill.pdf">Slides</a>]
                            </li><br>

                            <li>Create a Python Data Validation Engine Prototype<br>
                                Sponsored by <font color="#800080">Forcepoint</font><br>
                                <b> Industrial Mentor:</b>
                                <font color="green">Dustin Endres</font><br>
                                [<a href="Projects/SE4485-Dustin.pdf">Description</a>]
                                [<a href="Projects/Slides-Investigate-seccomp-Usage-in-Networking-Enabled-Applications-Dustin-Endres.pdf">Slides</a>]
                            </li><br>

                            <li>Phase I - Real-time Remote Viewing Application<br>
                                Sponsored by <font color="#800080">uARexpert</font><br>
                                <b>Industrial Mentor:</b>
                                <font color="green">Dave Gibson</font><br>
                                [<a href="Projects/SE4485-Dave.pdf">Description</a>]
                                [<a href="Projects/Slides-Property-Assessment-Application-Dave-Gibson.pdf">Slides</a>]
                            </li><br>

                            <li>Multi-LLM Agile Feedback and Deliverables Accelerator <br>
                                Sponsored by <font color="#800080">ARGO</font><br>
                                <b> Industrial Mentor:</b>
                                <font color="green">Mark Bentsen</font><br>
                                [<a href="Projects/SE4485-Mark-1.pdf">Description</a>]
                                [<a href="Projects/Slides-Automated-Environment-Setup-Mark-Bentsen.pdf">Slides</a>]
                            </li><br>

                            <li>Scheduler <br>
                                Sponsored by <font color="#800080">ARGO</font><br>
                                <b> Industrial Mentors:</b>
                                <font color="green">Mark Bentsen, Reza Aghaei, Raisa Gonzalez, and Ponchai Reainthong</font><br>
                                [<a href="Projects/SE4485-Mark-2.pdf">Description</a>]
                                [<a href="Projects/Slides-Automated-Environment-Setup-Mark-Bentsen.pdf">Slides</a>]
                            </li><br>

                            <li>Text-to-speech Synthesis <font color="red"> (NDA required)</font><br>
                                Sponsored by <font color="#800080">NTT DATA Services</font><br>
                                <b> Industrial Mentors:</b>
                                <font color="green">Vivekanand Rangaswamy and Chetan Mepani</font><br>
                                [<a href="Projects/SE4485-Vivekanand&Sabrina-1.pdf">Description</a>]
                                [<a href="Projects/Slides-Develop-a-MS-Visio-Based-Network-Simulator-Thomas-Hill.pdf">Slides</a>]
                            </li><br>

                            <li>Develop a New Minimum Viable Products Software Development Process<br>
                                Sponsored by <font color="#800080">The Fellows Consulting Group (FCG)</font><br>
                                <b> Industrial Mentor:</b>
                                <font color="green">Tom Hill</font><br>
                                [<a href="Projects/SE4485-Tom-1.pdf">Description</a>]
                                 [<a href="Projects/Slides-Develop-a-CIObrain-Application-Deployment-Capability-Thomas-Hill.pdf">Slides</a>]
                            </li><br>

                            <li>Design, Develop and Test a Simple Prototype GUI for the Windows-version of the Intrusion Detection App Snort<br>
                                Sponsored by <font color="#800080">The Fellows Consulting Group (FCG)</font><br>
                                <b> Industrial Mentor:</b>
                                <font color="green">Tom Hill</font><br>
                                [<a href="Projects/SE4485-Tom-2.pdf">Description</a>]
                                [<a href="Projects/Slides-Develop-a-MS-Visio-Based-Network-Simulator-Thomas-Hill.pdf">Slides</a>]
                            </li><br>

                            <li>Development of a Single Points of Failure Analysis Tool<br>
        Sponsored by <font color="#800080">The Fellows Consulting Group (FCG)</font><br>
        <b> Industrial Mentor:</b> <font color="green">Tom Hill</font><br>
        [<a href="Projects/Develop-a-Single-Points-of-Failure-Analysis-Tool-Thomas-Hill.pdf">Description</a>]
        <!-- [<a href="Projects/Slides-Develop-a-MS-Visio-Based-Network-Simulator-Thomas-Hill.pdf">Slides</a>] -
    </li><br>-->

                            <!--<li>Internet Research Assistant<br>
        Sponsored by <font color="#800080">The Fellows Consulting Group (FCG)</font><br>
        <b> Industrial Mentor:</b> <font color="green">Jeff Buchmiller</font><br>
        [<a href="Projects/Internet-Research-Assistant-Jeff-Buchmiller.pdf">Description</a>]
        <!-- [<a href="Projects/Slides-Automated-Data-Enhancer-Jeffry-Buchmiller.pdf">Slides</a>]
    </li><br>-->

                            <!-- <li>Deploy a SmartPhone Verify ID Facial Recognition Application to Android Store and Azure<br>
        Sponsored by <font color="#800080">The Fellows Consulting Group (FCG)</font><br>
        <b> Industrial Mentor:</b> <font color="green">Tom Hill</font><br>
        [<a href="Projects/Depoly-verifyID-Facial-Recognition-Android-App-Tom-Hill.pdf">Description</a>]
        [<a href="Projects/Slides-Depoly-verifyID-Facial-Recognition-Android-App-Tom-Hill.pdf">Slides</a>]
    </li><br> -->

                            <!-- <li>Item Generator<br>
        Sponsored by <font color="#800080">ARGO</font><br>
        <b> Industrial Mentor:</b> <font color="green">Mark Bentsen</font><br>
        [<a href="Projects/Item Generator - Mark Bentsen.pdf">Description</a>]
        [<a href="Projects/Slides - ARGO - Mark Bentsen.pdf">Slides</a>]
    </li><br> -->

                            <!-- <li>Augmented Reality Satellite Viewer<br>
        Sponsored by <font color="#800080">Communications and Power Industries</font><br>
        <b> Industrial Mentor:</b> <font color="green">Luke Carr</font><br>
        [<a href="Projects/Augmented Reality Satellite Viewer - Luke Carr.pdf">Description</a>]
        [<a href="Projects/Build CIObrain [asset management] Web Application - Slides - Tom Hill.pdf">Slides</a>]
    </li><br> -->

                            <!-- <li>Media Stream Data Visualization<br>
        Sponsored by <font color="#800080">MediaKind</font><br>
        <b> Industrial Mentor:</b> <font color="green">Jeff Buchmiller</font><br>
        [<a href="Projects/Media Stream Data Visualization - Jeffry Buchmiller.pdf">Description</a>]
        [<a href="Projects/Slides - Media Stream Data Visualization - Jeffry Buchmiller.pdf">Slides</a>]
    </li><br> -->

                            <!-- <li>Develop a SmartPhone Verify ID Facial Recognition Application<br>
        Sponsored by <font color="#800080">The Fellows Consulting Group (FCG)</font><br>
        <b> Industrial Mentor:</b> <font color="green">Tom Hill</font><br>
        [<a href="Projects/Develop a SmartPhone Verify ID Facial Recognition Application - Tom Hill.pdf">Description</a>]
        [<a href="Projects/Develop a SmartPhone Verify ID Facial Recognition Application - Slides - Tom Hill.pdf">Slides</a>]
    </li><br>

    <li>Contextual Search via Voice<br>
        Sponsored by <font color="#800080">Amazon</font><br>
        <b> Industrial Mentor:</b> <font color="green">Jack Story</font><br>
        [<a href="Projects/Contextual Search via Voice - Jack Story.pdf">Description</a>]
        [<a href="Projects/Contextual Search via Voice - Slides - Jack Story.pdf">Slides</a>]
    </li><br>    

    <li>Qt GUIs for Embedded Systems<br>
        Sponsored by <font color="#800080">Communications and Power Industries</font><br>
        <b> Industrial Mentor:</b> <font color="green">Luke Carr</font><br>
        [<a href="Projects/Product Line Engineering Research and Implementation - Luke Carr.pdf">Description</a>]
    </li><br>

    <li>Remote Clinician Verification<br>
        Sponsored by <font color="#800080">BSecurity</font><br>
        <b> Industrial Mentor:</b> <font color="green">Ed Kettler</font><br>
        [<a href="Projects/Remote Clinician Verification - Ed Kettler.pdf">Description</a>]
        [<a href="Projects/Remote Clinician Verification - Slides - Ed Kettler.pdf">Slides</a>]
    </li><br>

    <li>Item Generator<br>
        Sponsored by <font color="#800080">ARGO</font><br>
        <b> Industrial Mentor:</b> <font color="green">Mark Bentsen</font><br>
        [<a href="Projects/Item Generator - Mark Bentsen.pdf">Description</a>]
        [<a href="Projects/Item Generator - Slides - Mark Bentsen.pdf">Slides</a>]
    </li><br>

    <li>Media Streaming Manager<br>
        Sponsored by <font color="#800080">MediaKind</font><br>
        <b> Industrial Mentor:</b> <font color="green">Jeffry Buchmiller</font><br>
        [<a href="Projects/Media Streaming Manager - Jeffry Buchmiller.pdf">Description</a>]
        [<a href="Projects/Media Streaming Manager - Presentation Slides - Jeffry Buchmiller.pdf">Slides</a>]
    </li><br>
-->
                        </ul>
                    </blockquote>
                    <!------------------------------------------------------------------------------------>
                    <p><b>
                            <font color="#3333FF" , size="+1">Group Organization &amp; Project Assignment</font>
                        </b></p>
                    <blockquote>
                        <ul>
                            <li>
                                <!-- <a href="Group-Organization/2023-Spring-SE4485-Group-Organization.pdf" target="_blank">[pdf]</a> -->
                                <!--<a href="#" target="_blank">List of students</a>-->
                                <a href="Group-Organization/SE4485-Spring-2024-Student-List.pdf" target="_blank">List of students</a>
                            </li>
                            <li>
                                <!--<a href="#" target="_blank">Group Organization</a>-->
                                <a href="Group-Organization/SE4485-Spring-2024-Project-Assignment.pdf" target="_blank">Group Organization & Project Assignment</a>
                                <!-- <a href="" target="_blank">TBA</a> -->
                            </li>
                        </ul>
                    </blockquote>
                    <!------------------------------------------------------------------------------->
                    <p>
                        <b>
                            <font color="#3333FF">
                                <font size="+1">Document Templates & Due Dates</font>
                            </font>
                        </b>
                    </p>

                    <blockquote>
                        <p style="text-align: justify;">
                            The following templates are used to help students incorporate appropriate engineering standards and multiple constraints, wherever appropriate.
                            They give students an opportunity to apply the knowledge and skills acquired in earlier coursework to their capstone projects.
                        </p>
                        <p style="text-align: justify;">
                            Students should refer to the standards in the <a href="#standards"><i>Engineering Standards</i></a> section as the guidelines while working on their projects.
                            These standards are the basis for creating the default templates.
                            In addition, feedback from industry mentors and former students, as well as lessons learned from previous semesters, are also taken into consideration.
                            Depending on the nature of a project, these templates can be customized after receiving approval from the industry mentors and faculty advisors.
                        </p>

                        <table style="font-weight:bold;">
                            <tbody>
                                <tr>
                                    <td colspan="2">
                                        <ul style="margin-block-start: 0; margin-block-end: 0;">
                                            <li>
                                                Submit all the deliverables via <font color="red"> eLearning</font>.<br>
                                                All submissions must be in <font color="red">Word .docx</font> format.
                                            </li>
                                        </ul>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <ul style="margin-block-start: 0; margin-block-end: 0;">
                                            <li> Project Management Plan <a href="./Template/Word/Project Management Plan.docx" target="_blank">[docx]</a> </li>
                                        </ul>
                                    </td>
                                    <td>Due: February 2, 2024 (CDT)</td>
                                </tr>

                                <tr>
                                    <td colspan="2" style="padding-left: 16px;">
                                        Additional information about configuration management <a href="./Template/CM-Tool.pdf" target="_blank">[pdf]</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <ul style="margin-block-start: 0; margin-block-end: 0;">
                                            <li>Requirements Documentation <a href="./Template/Word/Requirements Documentation.docx" target="_blank">[docx]</a></li>
                                        </ul>
                                    </td>
                                    <td>Due: February 23, 2024 (CDT)</td>
                                </tr>

                                <tr>
                                    <td>
                                        <ul style="margin-block-start: 0; margin-block-end: 0;">
                                            <li>Architecture Documentation <a href="./Template/Word/Architecture Documentation.docx" target="_blank">[docx]</a></li>
                                        </ul>
                                    </td>
                                    <td>Due: March 15, 2024 (CDT)</td>
                                </tr>

                                <tr>
                                    <td>
                                        <ul style="margin-block-start: 0; margin-block-end: 0;">
                                            <li>Detailed Design Documentation <a href="./Template/Word/Detailed Design Documentation.docx" target="_blank">[docx]</a> </li>
                                        </ul>
                                    </td>
                                    <td>Due: March 29, 2024 (CDT)</td>
                                </tr>

                                <tr>
                                    <td>
                                        <ul style="margin-block-start: 0; margin-block-end: 0;">
                                            <li>Test Plan <a href="./Template/Word/Test Plan.docx" target="_blank">[docx]</a></li>
                                        </ul>
                                    </td>
                                    <td>Due: April 19, 2024 (CDT)</td>
                                </tr>

                                <tr>
                                    <td>
                                        <ul style="margin-block-start: 0; margin-block-end: 0;">
                                            <li>Final Project Demonstration (Slides & Demo)</li>
                                        </ul>
                                    </td>
                                    <td>Due: May 1, 2024 (CDT)</td>
                                </tr>

                                <tr>
                                    <td>
                                        <ul style="margin-block-start: 0; margin-block-end: 0;">
                                            <li>Final Project Report <a href="./Template/Word/Final Project Report.docx" target="_blank">[docx]</a></li>
                                        </ul>
                                    </td>
                                    <td>Due: May 3, 2024 (CDT)</td>
                                </tr>

                            </tbody>
                        </table>
                    </blockquote>

                    <p><b>
                            <font color="#3333FF">
                                <font size="+1">Final Project Presentation</font>
                            </font>
                        </b>
                    </p>
                    <blockquote>
                        <ul>

                            <li>Friday, May 3 (1:00 pm - 4:00 pm, CDT), 2024 in <font color='red'>ECSW 1.365 </font>
                                <!-- [<a href="./Final-Presentation/Schedule/2023-12-01-SE4485-Final-Presentation-Schedule.pdf" target="_blank">Schedule</a>]-->
                            </li>
                        </ul>
                    </blockquote>

                    <p id="standards">
                        <b>
                            <font color="#3333FF">
                                <font size="+1">Engineering Standards</font>
                            </font>
                        </b>
                    </p>
                    <blockquote>
                        <ul>
                            <li>IEEE Std 1058-1998: Software Project Management Plans [<a href="./IEEE/IEEE-Std-1058-1998-Software-Project-Management-Plans.pdf" target="_blank">pdf</a>]</li>
                            <li>PMBOK® Guide: Project Management Body of Knowledge [<a href="./IEEE/PMBOKR.pdf" target="_blank">pdf</a>]</li>
                            <li>IEEE Std 12207: Software Life Cycle Processes [<a href="./IEEE/IEEE 12207 (2017) - Software Life Cycle Processes.pdf" target="_blank">pdf</a>]</li>
                            <li>IEEE Std 15939: Measurement Process [<a href="./IEEE/IEEE 15939 (2017) - Measurement Process.pdf" target="_blank">pdf</a>]</li>
                            <li>IEEE Std 830-1998: Software Requirements [<a href="./IEEE/IEEE Std 830-1998-Software-Requirements.pdf" target="_blank">pdf</a>]</li>
                            <li>IEEE Std 29148: Requirements Engineering [<a href="./IEEE/IEEE 29148 (2011) - Requirements Engineering.pdf" target="_blank">pdf</a>]</li>
                            <li>IEEE Std 1471-2000: Software Architecture [<a href="./IEEE/IEEE-Std-1471-2000-Software-Architecture.pdf" target="_blank">pdf</a>]</li>
                            <li>IEEE Std 1016-1998-(Revision-2009): Software Design [<a href="./IEEE/IEEE-Std-1016-1998-(Revision-2009)-Software-Design.pdf" target="_blank">pdf</a>]</li>
                            <li>IEEE Std 829-1983: Software Testing [<a href="./IEEE/IEEE Std 829-1983-Software-Testing.pdf" target="_blank">pdf</a>]</li>
                            <li>ISO/IEC/IEEE Std 29119-1-(Revision-2022): Part 1 - Software Testing General Concepts [<a href="./IEEE/IEEE-Std-29119-1-(Revision-2022)-Software-Testing-General-Concepts.pdf" target="_blank">pdf</a>]</li>
                            <li>ISO/IEC/IEEE Std 29119-2-(Revision-2021): Part 2 - Test Process [<a href="./IEEE/IEEE-Std-29119-2-(Revision-2021)-Test-Process.pdf" target="_blank">pdf</a>]</li>
                            <li>ISO/IEC/IEEE Std 29119-3-(Revision-2021): Part 3 - Test Documentation [<a href="./IEEE/IEEE-Std-29119-3-(Revision-2021)-Test-Documentation.pdf" target="_blank">pdf</a>]</li>
                            <li>ISO/IEC/IEEE Std 29119-4-(Revision-2021): Part 4 - Test Techniques [<a href="./IEEE/IEEE 29119.4 (2021) - Test Techniques.pdf" target="_blank">pdf</a>]</li>
                            <li>ISO/IEC/IEEE Std 29148-2018: Systems and Software Engineering — Life Cycle Processes — Requirements Engineering [<a href="./IEEE/ISO-IEC-IEEE-29148-2018.pdf" target="_blank">pdf</a>]</li>
                            <li>ISO/IEC/IEEE Std 42030:2019: Software, Systems and Enterprise - Architecture Evaluation Framework [<a href="./IEEE/ISO-IEC-IEEE-42030-2019.pdf" target="_blank">pdf</a>]</li>
                            <!--<li>Writing Effective <a href="http://www.gatherspace.com/static/use_case_example.html" target="_blank">Use Case</a></li>-->

                            <li>Larson, E. and Gray, C., 2014. <i>Project Management: The Managerial Process.</i> McGraw Hill</li>
                            <li>Humphrey, W.S. and Thomas, W.R., 2010. <i>Reflections on Management: How to Manage Your Software Projects, Your Teams, Your Boss, and Yourself.</i> Pearson Education</li>
                            <li>Lamsweerde, A.V., 2009. <i>Requirements Engineering: From System Goals to UML Models to Software Specifications.</i> John Wiley</li>
                            <li>Lattanze, A.J., 2008. <i>Architecting Software Intensive Systems: A Practitioner’s Guide.</i> CRC Press</li>
                            <li>Bass, L., Clements, P. and Kazman, R., 2003. <i>Software Architecture in Practice.</i> Addison-Wesley</li>
                            <li>Larman, C., 2012. <i>Applying UML and Patterns: An Introduction to Object Oriented Analysis and Design and Iterative Development.</i> Pearson Education</li>
                            <li>Hyman, B., 1998. <i>Fundamentals of Engineering Design.</i> New Jersey: Prentice Hall</li>
                            <li>Simon, H.A., 2014. <i>A Student's Introduction to Engineering Design: Pergamon Unified Engineering Series</i> (Vol. 21). Elsevier</li>
                            <li>Jorgensen, P.C., 2013. <i>Software Testing: A Craftsman's Approach.</i> Auerbach Publications</li>
                            <li>Mathur, A.P., 2013. <i>Foundations of Software Testing, 2/e.</i> Pearson Education</li>

                        </ul>
                    </blockquote>

                    <p id="tutorials">
                        <b>
                            <font color="#3333FF">
                                <font size="+1">Tutorials, UML, Configuration Management, and Database</font>
                            </font>
                        </b>
                    </p>
                    <blockquote>
                        <ul>
                            <li>Tutorial on <a href="https://docs.python.org/3/tutorial/" target="_blank">Python</a> (Script Language)</li>
                            <li>Tutorial on <a href="https://www.tutorialspoint.com/jsp/index.htm" target="_blank">JSP</a> (JavaServer Pages)</li>
                            <li>Tutorial on <a href="https://www.tutorialspoint.com/perl/" target="_blank">Perl</a> (Practical Extraction and Report Language)</li>
                            <li>Tutorial on <a href="http://us3.php.net/manual/en/tutorial.php" target="_blank">PHP</a> (Hypertext Preprocessor)</li>
                            <li style="margin-bottom: 1em;">Tutorial on <a href="http://www.w3schools.com/asp/default.asp" target="_blank">ASP</a> (Active Server Pages)</li>

                            <li><a href="http://www.uml-diagrams.org/use-case-diagrams.html" target="_blank">Use Case Diagrams</a></li>
                            <li><a href="https://www.smartdraw.com/class-diagram/" target="_blank">Class Diagrams</a></li>
                            <li><a href="http://www.agilemodeling.com/artifacts/sequenceDiagram.htm" target="_blank">Sequence Diagrams</a></li>
                            <li><a href="http://www.sdl-forum.org/MSC/msctutorial.pdf" target="_blank">Message Sequence Charts (MSC)</a></li>
                            <li><a href="http://agilemodeling.com/style/collaborationDiagram.htm" target="_blank">Collaboration Diagrams</a></li>
                            <li style="margin-bottom: 1em;"><a href="https://www.smartdraw.com/state-diagram/" target="_blank">Statechart Diagrams</a></li>

                            <li><a href="https://en.wikipedia.org/wiki/Configuration_management" target="_blank">Configuration Management (CM)</a></li>
                            <li>Free <a href="https://git-scm.com/" target="_blank">Source Code Version Control</a> Software</li>
                            <li>Tutorial on CVS [<a href="./Tutorial/CVS.pdf" target="_blank">pdf</a>]</li>
                            <li>Tutorial on <a href="http://dev.mysql.com/doc/refman/5.0/en/tutorial.html" target="_blank">MySQL</a></li>
                        </ul>
                    </blockquote>

                    <p id="professional">
                        <b>
                            <font color="#3333FF">
                                <font size="+1">Professional and Technical Communication</font>
                            </font>
                        </b>
                    </p>
                    <blockquote>
                        <ul>
                            <li>Technical writing reference book <a href="http://www.chicagomanualofstyle.org/home.html" target="_blank">Chicago Manual of Style</a></li>
                            <li>Lecture on <a href="./00-Technical.blade.php" target="_blank">Technical Writing</a></li>
                            <li><a target="_blank" href="https://www.studocu.com/en-us/course/the-university-of-texas-at-dallas/professional-and-technical-communication/2616822">Lecture Notes for ECS 3390</a></li>
                        </ul>
                    </blockquote>


                    <div hidden>
                        <blockquote>
                            <li><a href="https://en.wikipedia.org/wiki/Configuration_management" target="_blank">Configuration Management (CM)</a></li>
                            <li>Free <a href="https://git-scm.com/" target="_blank">Source Code Version Control</a> Software</li>
                            <li>Tutorial on CVS [<a href="./Tutorial/CVS.pdf" target="_blank">pdf</a>]</li>
                            <li>Tutorial on <a href="http://dev.mysql.com/doc/refman/5.0/en/tutorial.html" target="_blank">MySQL</a></li>
                            <!--<li>Writing Effective <a href="http://www.gatherspace.com/static/use_case_example.html" target="_blank">Use Case</a></li>-->
                            <li>More tutorials on creating UML diagrams by using Rose</li>
                        </blockquote>
                        <!-------------------------------------------------------------------------->
                        <blockquote>
                            <li> <a href="http://www.uml-diagrams.org/use-case-diagrams.html" target="_blank">Use Case Diagrams</a></li>
                            <li> <a href="https://www.smartdraw.com/class-diagram/" target="_blank">Class Diagrams</a></li>
                            <li> <a href="http://www.agilemodeling.com/artifacts/sequenceDiagram.htm" target="_blank">Sequence Diagrams</a></li>
                            <li> <a href="http://www.sdl-forum.org/MSC/msctutorial.pdf" target="_blank">Message Sequence Charts (MSC)</a></li>
                            <li> <a href="http://agilemodeling.com/style/collaborationDiagram.htm" target="_blank">Collaboration Diagrams</a></li>
                            <li> <a href="https://www.smartdraw.com/state-diagram/" target="_blank">Statechart Diagrams</a></li>
                        </blockquote>
                        <!-------------------------------------------------------------------------->

                        <blockquote>

                            <li>Tutorial on <a href="https://docs.python.org/3/tutorial/" target="_blank">Python</a> (Script Language)</li>
                            <li>Tutorial on <a href="https://www.tutorialspoint.com/jsp/index.htm" target="_blank">JSP</a> (JavaServer Pages)</li>
                            <li>Tutorial on <a href="https://www.tutorialspoint.com/perl/" target="_blank">Perl</a> (Practical Extraction and Report Language)</li>
                            <li>Tutorial on <a href="http://us3.php.net/manual/en/tutorial.php" target="_blank">PHP</a> (Hypertext Preprocessor)</li>
                            <li>Tutorial on <a href="http://www.w3schools.com/asp/default.asp" target="_blank">ASP</a> (Active Server Pages)</li>
                            <li>Technical writing reference book <a href="http://www.chicagomanualofstyle.org/home.html" target="_blank">Chicago Manual of Style</a></li>
                            <li>Lecture on <a href="https://paris.utdallas.edu/reu/page/technical" target="_blank">Technical Writing</a></li>

                        </blockquote>
                    </div>


                    <p>

                        <!--<p><b><font color="#3333FF"><font size="+1">Some Useful Resources</font></font></b>
</p>
<blockquote>
    <li>IEEE Std 1058-1998-Software-Project-Management-Plans [<a href="./IEEE/IEEE-Std-1058-1998-Software-Project-Management-Plans.pdf" target="_blank">pdf</a>]</li>
    <li>IEEE Std 830-1998-Software-Requirements [<a href="./IEEE/IEEE Std 830-1998-Software-Requirements.pdf" target="_blank">pdf</a>]</li>
    <li>IEEE Std 1471-Software-Architecture [<a href="./IEEE/IEEE-Std-1471-Software-Architecture.pdf" target="_blank">pdf</a>]</li>
    <li>IEEE Std 1016-1998-(Revision-2009)-Software-Design [<a href="./IEEE/IEEE-Std-1016-1998-(Revision-2009)-Software-Design.pdf" target="_blank">pdf</a>]</li>
	<li>IEEE Std 829-1983-Software-Testing [<a href="./IEEE/IEEE Std 829-1983-Software-Testing.pdf" target="_blank">pdf</a>]</li>
	<li>IEEE Std 129119-1-(Revision-2022)-Software-Testing-General-Concepts [<a href="./IEEE/IEEE-Std-29119-1-(Revision-2022)-Software-Testing-General-Concepts.pdf" target="_blank">pdf</a>]</li>
	<li>IEEE Std 129119-2-(Revision-2021)-Test-Process [<a href="./IEEE/IEEE-Std-29119-2-(Revision-2021)-Test-Process.pdf" target="_blank">pdf</a>]</li>
	<li>IEEE Std 129119-3-(Revision-2021)-Test-Documentation [<a href="./IEEE/IEEE-Std-29119-3-(Revision-2021)-Test-Documentation.pdf" target="_blank">pdf</a>]</li>
    <!--<li>Writing Effective <a href="http://www.gatherspace.com/static/use_case_example.html" target="_blank">Use Case</a></li>--
</blockquote>-->

                        </ul>
                        </blockquote>
                    <p><br>

                        <hr>
                        <br />
                        <a href="./"> [Logoff] </a>
                        <!------------------------------------------------------------------------->
                    <?php
                } else {
                    ?>
                        Login Failed!
                        <br />
                        <a href="./"> [Back] </a>
                    <?php
                }
                    ?>
</body>

</html>